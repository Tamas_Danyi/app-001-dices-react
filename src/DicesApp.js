import React, { Component } from 'react';
import DiceChoosers from './DiceChoosers';
import Rolls from './Rolls';
import DiceConfig from './DiceConfig';
import 'bootstrap/dist/css/bootstrap.css';
import './DicesApp.css';

class DicesApp extends Component {
  state = {
    diceConfigs: [
      new DiceConfig(2, 0),
      new DiceConfig(4, 0),
      new DiceConfig(6, 2),
      new DiceConfig(8, 0),
      new DiceConfig(10, 0),
      new DiceConfig(12, 0),
      new DiceConfig(20, 0),
      new DiceConfig(100, 0)
    ]
  }

  handleAddDice = diceType => {
    this.state.diceConfigs
      .filter(diceConfig => diceConfig.diceType === diceType)
      .forEach(diceConfig => diceConfig.diceCount++);
    this.setState({ diceConfigs: this.state.diceConfigs });
  }

  handleRemoveDice = diceType => {
    this.state.diceConfigs
      .filter(diceConfig => diceConfig.diceType === diceType)
      .forEach(diceConfig => {
        if (diceConfig.diceCount > 0) {
          diceConfig.diceCount--;
        }
      });
    this.setState({ diceConfigs: this.state.diceConfigs });
  }

  handleResetAllToZero = () => {
    this.state.diceConfigs.forEach(diceConfig => diceConfig.diceCount = 0);
    this.setState({ diceConfigs: this.state.diceConfigs });
  }

  handleReRoll = () => {
    this.setState({ diceConfigs: this.state.diceConfigs });
  }

  render() {
    return (
      <React.Fragment>
        <DiceChoosers
          diceConfigs={this.state.diceConfigs}
          onAddDice={this.handleAddDice}
          onRemoveDice={this.handleRemoveDice}
          onResetAllToZero={this.handleResetAllToZero}
        />
        <Rolls diceConfigs={this.state.diceConfigs} onReRoll={this.handleReRoll} />
      </React.Fragment>
    )
  }
}

export default DicesApp;