import React, { Component } from 'react';
import DiceChooser from './DiceChooser';

class DiceChoosers extends Component {
  state = {
    showDiceChoosers: true,
    diceChooserTitle: 'Dice choosers',
    resetToZeroText: 'Reset all to zero'
  }

  renderDiceChoosers = () => {
    if (this.state.showDiceChoosers) {
      return this.props.diceConfigs.map(this.renderDiceChooser)
    }
  }

  renderDiceChooser = (diceConfig) => {
    return (
      <div key={'div_' + diceConfig.diceType}>
        <DiceChooser key={'diceChooser_' + diceConfig.diceType}
          diceConfig={diceConfig}
          onAddDice={() => this.props.onAddDice(diceConfig.diceType)}
          onRemoveDice={() => this.props.onRemoveDice(diceConfig.diceType)} />
      </div>
    );
  }

  toggleDiceChoosers = () => {
    this.setState({ showDiceChoosers: !this.state.showDiceChoosers });
  }

  renderResetButton = () => {
    if (this.state.showDiceChoosers && this.hasAtLeastOneDice()) {
      return <button className='btn btn-warning btn-sm' onClick={this.props.onResetAllToZero}>{this.state.resetToZeroText}</button>
    }
  }

  hasAtLeastOneDice = () => {
    return this.props.diceConfigs && this.props.diceConfigs
      .filter(diceConfig => diceConfig.diceCount > 0)
      .length > 0;
  }

  render() {
    if (this.props.diceConfigs) {
      return (
        <div id='diceChoosersArea'>
          <h3 className='' onClick={this.toggleDiceChoosers}>{this.state.diceChooserTitle}</h3>
          {this.renderDiceChoosers()}
          {this.renderResetButton()}
        </div>
      )
    }
    return '';
  }
}

export default DiceChoosers;