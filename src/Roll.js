import React, { Component } from 'react';

class Roll extends Component {
  state = {
    diceRollLabelText: 'd%s-rolls: '
  }

  renderLabel = () => {
    return this.state.diceRollLabelText.replace('%s', this.props.diceRollNumber.diceType);
  }

  render = () => {
    if (this.props.diceRollNumber
      && this.props.diceRollNumber.diceType
      && this.props.diceRollNumber.numbers
      && this.props.diceRollNumber.numbers.length > 0) {

      return (
        <div>
          {this.renderLabel()}{this.props.diceRollNumber.numbers.join(', ')}
        </div>
      );
    }
    return '';
  }
}

export default Roll;