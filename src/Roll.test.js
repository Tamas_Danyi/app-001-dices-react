import React from 'react';
import ReactDOM from 'react-dom';
import Roll from './Roll';
import DiceRollNumber from './DiceRollNumber';
import DiceConfig from './DiceConfig';

it('renders without crashing', () => {
  const diceConfig = new DiceConfig(2, 4);
  const diceRollNumberData = new DiceRollNumber(diceConfig);

  const div = document.createElement('div');
  ReactDOM.render(<Roll diceRollNumber={diceRollNumberData} />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('renders without crashing - without property', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Roll />, div);
  ReactDOM.unmountComponentAtNode(div);
});