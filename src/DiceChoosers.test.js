import React from 'react';
import ReactDOM from 'react-dom';
import DiceChoosers from './DiceChoosers';
import DiceConfig from './DiceConfig';

it('renders without crashing', () => {
  const diceConfigData = [
    new DiceConfig(2, 4),
    new DiceConfig(4, 2)
  ];

  const div = document.createElement('div');
  ReactDOM.render(<DiceChoosers diceChoosers={diceConfigData} />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('renders without crashing - without property', () => {
  const div = document.createElement('div');
  ReactDOM.render(<DiceChoosers />, div);
  ReactDOM.unmountComponentAtNode(div);
});