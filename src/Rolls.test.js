import React from 'react';
import ReactDOM from 'react-dom';
import Rolls from './Rolls';
import DiceConfig from './DiceConfig';

it('renders without crashing', () => {
  const diceConfigsData = [
    new DiceConfig(2, 4),
    new DiceConfig(4, 0),
    new DiceConfig(6, 4)
  ];
    
  const div = document.createElement('div');
  ReactDOM.render(<Rolls diceConfigs={diceConfigsData} />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('renders without crashing - without property', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Rolls />, div);
  ReactDOM.unmountComponentAtNode(div);
});