import React, { Component } from 'react';
import Roll from './Roll';
import DiceRollNumber from './DiceRollNumber';

class Rolls extends Component {
  state = {
    rollAreaTitle: 'Rolls',
    rollAgainButtonText: 'Roll again'
  }

  renderRolls() {
    return this.props.diceConfigs
      .filter(diceConfig => diceConfig.diceCount > 0)
      .map(diceConfig => new DiceRollNumber(diceConfig))
      .map(this.renderRoll);
  }

  renderRoll(diceRollNumber) {
    return (
      <Roll key={'roll_' + diceRollNumber.diceType} diceRollNumber={diceRollNumber} />
    );
  }

  hasToRender() {
    return this.props.diceConfigs
      && this.props.diceConfigs
        .filter(diceConfig => diceConfig.diceCount > 0)
        .length > 0;
  }

  render = () => {
    if (this.hasToRender()) {
      return (
        <div id='diceRollsArea'>
          <h3>{this.state.rollAreaTitle}</h3>
          {this.renderRolls()}
          <button className='btn btn-primary' onClick={this.props.onReRoll}>{this.state.rollAgainButtonText}</button>
        </div>
      );
    }
    return '';
  }

}

export default Rolls;