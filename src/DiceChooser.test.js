import React from 'react';
import ReactDOM from 'react-dom';
import DiceChooser from './DiceChooser';
import DiceConfig from './DiceConfig';

it('renders without crashing', () => {
  const diceConfig = new DiceConfig(2, 4);

  const div = document.createElement('div');
  ReactDOM.render(<DiceChooser diceConfig={diceConfig} />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('renders without crashing - without property', () => {
  const div = document.createElement('div');
  ReactDOM.render(<DiceChooser />, div);
  ReactDOM.unmountComponentAtNode(div);
});