import React from 'react';
import ReactDOM from 'react-dom';
import DicesApp from './DicesApp';

it('renders without crashing - without property', () => {
  const div = document.createElement('div');
  ReactDOM.render(<DicesApp />, div);
  ReactDOM.unmountComponentAtNode(div);
});