import React, { Component } from 'react';

class DiceChooser extends Component {

  render = () => {
    if (this.props.diceConfig) {
      return (
        <span>
          d{this.props.diceConfig.diceType}:
         <button className='btn btn-primary btn-sm'
            onClick={this.props.onRemoveDice}>-</button>
          {this.props.diceConfig.diceCount}
          <button className='btn btn-primary btn-sm'
            onClick={this.props.onAddDice}>+</button>
        </span>
      )
    }
    return ''
  }
}

export default DiceChooser;