class DiceConfig {

  constructor(diceType, diceCount) {
    this.diceType = diceType;
    this.diceCount = diceCount;
  }
}

export default DiceConfig;